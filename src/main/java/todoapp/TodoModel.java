package todoapp;

public class TodoModel {

    private static int cont = 0;
    public int Id;
    public String Title;
    public boolean Done;

    public TodoModel() {
    }

    public TodoModel(String title) {
        Id = cont++;
        Title = title;
        Done = false;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public boolean isDone() {
        return Done;
    }

    public void setDone(boolean done) {
        Done = done;
    }
}
