package todoapp;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ArrayList<TodoModel> todoList = new ArrayList<>();

        try (Scanner sc = new Scanner(System.in)) {
            String decisao;

            do {
                System.out.println("Digite o que deseja fazer: ");
                System.out.println("1 - Adicionar Tarefa");
                System.out.println("2 - Listar Tarefas");
                System.out.println("3 - Finalizar Tarefa");
                System.out.println("4 - Sair");
                decisao = sc.nextLine();

                switch (decisao) {
                    case "1":
                        System.out.println("Digite o titulo da tarefa: ");
                        String titulo = sc.nextLine();
                        System.out.println("");
                        TodoModel todo = new TodoModel(titulo);
                        todoList.add(todo);
                        break;
                    case "2":
                        for (TodoModel todoModel : todoList) {
                            System.out.println("");
                            System.out.println("Id: " + todoModel.getId());
                            System.out.println("Titulo: " + todoModel.getTitle());
                            System.out.println("Finalizado: " + todoModel.isDone());
                            System.out.println("");
                        }
                        break;
                    case "3":
                        System.out.println("Digite o id da tarefa que deseja finalizar: ");
                        int id = sc.nextInt();
                        for (TodoModel todoModel : todoList) {
                            if (todoModel.getId() == id) {
                                todoModel.setDone(true);
                                System.out.println("Tarefa - " + todoModel.getTitle() + " - finalizada com sucesso!");
                                System.out.println("");
                            }
                        }
                        break;
                    case "4":
                        System.out.println("Saindo...");
                        break;
                    default:
                        System.out.println("Opção inválida");
                        break;
                }
            } while (!decisao.equals("4"));
        }
    }
}
