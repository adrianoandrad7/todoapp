package todoapp.test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import todoapp.TodoModel;
import static org.junit.jupiter.api.Assertions.*;

class TodoModelTest {

    private TodoModel todo;

    @BeforeEach
    void setUp() {
        todo = new TodoModel();
        todo.Id = 1;
        todo.Title = ".Net";
        todo.Done = true;
    }

    @Test
    void testeId() {
        assertEquals(1,todo.Id);
    }

    @Test
    void testeTitle() {
        assertEquals(".Net",todo.Title);
    }

    @Test
    void testeIsDone() {
        assertTrue(todo.Done);
    }
}